package main

import (
	"log"

	
	"github.com/rathvong/ra_vo/server"
	"github.com/rathvong/ra_vo/service/database"
)



func main() {

	db := database.Connect()
	defer db.Close()

	log.Fatal(server.Serve(db))

}
