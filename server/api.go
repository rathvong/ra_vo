package server

import (
	"log"
	"net/http"

	mw "github.com/go-zoo/claw/middleware"
	"github.com/gorilla/mux"
	"github.com/thedevsaddam/renderer"

	"github.com/rathvong/ra_vo/service/database"
	"github.com/rathvong/ra_vo/service/middleware"
)

const (
	//Default port
	DefaultPort = "8000"
)

type Server struct {
	db *database.DB
}

var rnd *renderer.Render

// Parse and add all the html templates into renderer.
// Since the pages are defined by a tag, I can simply call them
// on the fly.
func init() {
	opts := renderer.Options{
		ParseGlobPattern: "server/templates/*.html",
	}

	rnd = renderer.New(opts)
}

// Serve will start routing service for the apis
func Serve(db *database.DB) error {
	r := mux.NewRouter()

	s := Server{db}

	port := getPort()
	log.Print("Listening on port " + port + " ... ")

	// Since Google is reviewing my app for oauth,
	// they are limiting my access to their API.
	// Please bare with me as I am sorting that.
	r.HandleFunc("/auth/google/login", s.GetGoogleLogin)
	r.HandleFunc("/auth/google/callback", s.GetGoogleCallback)

	r.HandleFunc("/", s.Home)

	r.HandleFunc("/signup", s.PostSignUp)
	r.HandleFunc("/signup/", s.GetSignUpPage)
	r.HandleFunc("/u/signin", s.PostSignIn)
	r.HandleFunc("/u/logout", s.GetLogout)

	r.HandleFunc("/onboarding/", middleware.Authenticate(db, http.HandlerFunc(s.GetOnboardingPage)))
	r.HandleFunc("/onboarding", middleware.Authenticate(db, http.HandlerFunc(s.PostOnboarding)))

	r.HandleFunc("/forgot", s.PostForgotLogin)
	r.HandleFunc("/forgot/", s.GetChangePasswordPage)

	r.HandleFunc("/u/resetpassword/", s.GetPasswordRecoveryPage)
	r.HandleFunc("/u/resetpassword", s.PostPasswordReset)

	r.HandleFunc("/profile", middleware.Authenticate(db, http.HandlerFunc(s.GetProfilePage)))

	r.HandleFunc("/policy", s.GetPolicy)

	r.Use(mw.Recovery, middleware.Log, middleware.NoCache)

	return http.ListenAndServe(port, r)
}
