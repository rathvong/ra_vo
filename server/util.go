package server

import (
	"encoding/json"
	"net/http"
	"os"
)

// We must retreive the default port given by Heroku.
// Heroku has dynamic ports.
func getPort() string {
	port := os.Getenv("PORT")
	if port == "" {
		return ":" + DefaultPort
	}

	return ":" + port
}

// Simple function to parse header request body.
func parseRequest(r *http.Request, v interface{}) error {
	defer r.Body.Close()
	return json.NewDecoder(r.Body).Decode(v)
}
