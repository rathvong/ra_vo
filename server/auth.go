package server

import (
	"errors"
	"net/http"
	"strings"
	"time"

	"github.com/rathvong/ra_vo/service/auth"
	"github.com/rathvong/ra_vo/service/users"
)

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// Data needed to display in the template our login views
type View struct {
	Error   string
	Success string
	User    users.User
}

// A cookie is validated when access the homepage to transfer
// a user forward to their profile.
func (s *Server) Home(w http.ResponseWriter, r *http.Request) {

	user, err := s.getLoggedInUser(r)

	if err == nil && user.ID > 0 {
		http.Redirect(w, r, "/profile", http.StatusFound)
		return
	}

	res := View{}
	rnd.HTML(w, http.StatusOK, "home", res)
}

func (s *Server) GetSignUpPage(w http.ResponseWriter, r *http.Request) {
	res := View{}

	rnd.HTML(w, http.StatusOK, "signup", res)
}

// Retrieve a logged in user from our cookie
func (s *Server) getLoggedInUser(r *http.Request) (*users.User, error) {
	t, err := r.Cookie("access_token")

	if t == nil || err != nil {
		if err == nil {
			err = errors.New("missing access token")
		}
		return nil, err
	}

	user, err := auth.GetUserByAccessToken(s.db, t.Value)

	if err != nil || user.ID == 0 {
		return nil, err
	}

	return user, nil
}

func (s *Server) PostSignIn(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodPost {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	var req LoginRequest

	r.ParseForm()

	res := View{}

	req.Email = r.FormValue("email")
	req.Password = r.FormValue("password")

	if req.Email == "" {
		res.Error = "missing email."
		rnd.HTML(w, http.StatusOK, "home", res)
		return
	}

	if req.Password == "" {
		res.Error = "missing password."
		rnd.HTML(w, http.StatusOK, "home", res)
		return
	}

	user, err := users.GetByEmail(s.db, strings.ToLower(req.Email))

	if err != nil {
		res.Error = "no user found."
		rnd.HTML(w, http.StatusOK, "home", res)
		return
	}

	if !users.CheckPasswordHash(user.EncryptedPassword, req.Password) {
		res.Error = "password does not match."
		rnd.HTML(w, http.StatusOK, "home", res)
		return
	}

	token := auth.Token{UserID: user.ID}

	if err := auth.CreateAccessToken(s.db, &token); err != nil {
		res.Error = "oops.... Something went wrong."
		rnd.HTML(w, http.StatusOK, "home", res)
		return
	}

	expiration := time.Now().Add(365 * 24 * time.Hour) // Set to expire in 1 year
	cookie := http.Cookie{Name: "access_token", Value: token.Token, Expires: expiration, Path: "/"}
	http.SetCookie(w, &cookie)

	http.Redirect(w, r, "/profile", http.StatusFound)

}

func (s *Server) PostSignUp(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodPost {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	var req LoginRequest

	res := View{}

	var password2 string

	r.ParseForm()

	req.Email = r.FormValue("email")
	req.Password = r.FormValue("password")
	password2 = r.FormValue("password2")

	if req.Email == "" {
		res.Error = "missing email"
		rnd.HTML(w, http.StatusOK, "signup", res)
		return
	}

	if req.Password == "" {
		res.Error = "missing password"
		rnd.HTML(w, http.StatusOK, "signup", res)
		return
	}

	if password2 != req.Password {
		res.Error = "password does not match"
		rnd.HTML(w, http.StatusOK, "signup", res)
		return
	}

	if len(req.Password) < 6 {
		res.Error = "password is too short. Required 6 characters"
		rnd.HTML(w, http.StatusOK, "signup", res)
		return
	}

	user := users.User{}

	user.Password = req.Password
	user.Email = strings.ToLower(req.Email)

	if exists, err := users.EmailExists(s.db, user.Email); err != nil || exists {

		if err != nil {
			res.Error = "an error occurred while creating your account."
			rnd.HTML(w, http.StatusOK, "signup", res)
			return
		}

		res.Error = "user already exists."
		rnd.HTML(w, http.StatusOK, "signup", res)

		return
	}

	err := users.Create(s.db, &user)

	if err != nil {
		res.Error = "error creating user."
		rnd.HTML(w, http.StatusOK, "signup", res)
		return
	}

	token := auth.Token{UserID: user.ID}

	if err := auth.CreateAccessToken(s.db, &token); err != nil {
		res.Error = "error completing signup"
		rnd.HTML(w, http.StatusOK, "signup", res)
		return
	}

	expiration := time.Now().Add(365 * 24 * time.Hour) // Set to expire in 1 year
	cookie := http.Cookie{Name: "access_token", Value: token.Token, Expires: expiration, Path: "/"}
	http.SetCookie(w, &cookie)

	http.Redirect(w, r, "/onboarding/", http.StatusFound)

}

// Expires our cookie and redirects to home
func (s *Server) GetLogout(w http.ResponseWriter, r *http.Request) {
	expiration := time.Now().Add(1 * time.Second) // Set to expire in one second
	cookie := http.Cookie{Name: "access_token", Value: "-1", Expires: expiration, Path: "/"}
	http.SetCookie(w, &cookie)

	http.Redirect(w, r, "/", http.StatusFound)
}
