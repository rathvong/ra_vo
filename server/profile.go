package server

import (
	"fmt"
	"log"
	"net/http"
	"regexp"

	"github.com/rathvong/ra_vo/service/middleware"

	"github.com/rathvong/ra_vo/service/users"
)

func (s *Server) GetProfilePage(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodGet {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	user, err := s.getLoggedInUser(r)

	if user == nil || err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	res := View{}

	res.User = *user

	for k, v := range middleware.NoCacheHeaders {
		w.Header().Set(k, v)
	}

	if err := rnd.HTML(w, http.StatusOK, "profile", res); err != nil {
		log.Printf("Error Parsing profile: %v", err)
	}
}

func (s *Server) GetOnboardingPage(w http.ResponseWriter, r *http.Request) {

	user, err := s.getLoggedInUser(r)

	if user == nil || err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	log.Printf("User: %+v", user)
	res := View{}
	res.User = *user

	for k, v := range middleware.NoCacheHeaders {
		w.Header().Set(k, v)

	}

	rnd.HTML(w, http.StatusOK, "onboarding", res)
}

func (s *Server) PostOnboarding(w http.ResponseWriter, r *http.Request) {

	user, err := s.getLoggedInUser(r)

	if user == nil || err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	r.ParseForm()

	user.Address = r.FormValue("address")
	user.Telephone = r.FormValue("telephone")
	user.FirstName = r.FormValue("first_name")
	user.LastName = r.FormValue("last_name")
	user.Email = r.FormValue("email")
	user.Password = r.FormValue("password")

	page := r.FormValue("page")

	res := View{}
	res.User = *user

	
	re := regexp.MustCompile(`^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$`)

	if !re.MatchString(user.Telephone) {
		res.Error = fmt.Sprint("Not a valid phone number format")

		if page == "onboarding" {
			rnd.HTML(w, http.StatusOK, "onboarding", res)
			return
		}

		rnd.HTML(w, http.StatusOK, "profile", res)
		return
	}

	//If password is too short this will show the profile page,

	if user.Password != "" && len(user.Password) < 6 {
		res.Error = "Password requires 6 characters"
		rnd.HTML(w, http.StatusOK, "profile", res)
		return
	}

	if err := users.Update(s.db, user); err != nil {
		res.Error = fmt.Sprint("Error: ", err)

		if page == "onboarding" {
			rnd.HTML(w, http.StatusOK, "onboarding", res)
			return
		}

		rnd.HTML(w, http.StatusOK, "profile", res)
		return
	}

	for k, v := range middleware.NoCacheHeaders {
		w.Header().Set(k, v)
	}

	http.Redirect(w, r, "/profile", http.StatusFound)

}
