package server

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/rathvong/ra_vo/service/auth"
	"github.com/rathvong/ra_vo/service/users"
)

const oauthGoogleUrlAPI = "https://www.googleapis.com/oauth2/v2/userinfo?access_token="

func (s *Server) GetGoogleLogin(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodGet {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	oauthState := auth.GenerateStateOauthCookie(w)
	u := auth.GoogleOauthConfig.AuthCodeURL(oauthState)
	http.Redirect(w, r, u, http.StatusTemporaryRedirect)
}

func (s *Server) GetGoogleCallback(w http.ResponseWriter, r *http.Request) {
	// Read oauthState from Cookie
	oauthState, _ := r.Cookie("oauthstate")

	res := View{}

	if r.Method != http.MethodGet {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	// oauthState is required to prevent CSRF attacks.
	if oauthState == nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	if r.FormValue("state") != oauthState.Value {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	data, err := auth.GetUserDataFromGoogle(r.FormValue("code"))
	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	var googleUser auth.GoogleUser
	err = json.Unmarshal(data, &googleUser)

	if err != nil {
		panic(err)
	}

	// We check for a valid user with a gmail account first
	// If one exists than we can update their google id
	exists, err := users.EmailExists(s.db, googleUser.Email)

	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	if exists {

		user, err := users.GetByEmail(s.db, googleUser.Email)

		if err != nil {
			log.Println(err)
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}
		if user.GoogleID == "" {

			user.GoogleID = googleUser.ID

			if err := users.Update(s.db, user); err != nil {
				log.Println(err)
				http.Redirect(w, r, "/", http.StatusFound)
				return

			}
		}

		token := auth.Token{UserID: user.ID}

		if err := auth.CreateAccessToken(s.db, &token); err != nil {
			log.Println(err)
			http.Redirect(w, r, "/", http.StatusFound)

			return
		}

		expiration := time.Now().Add(365 * 24 * time.Hour) // Set to expire in 1 year
		cookie := http.Cookie{Name: "access_token", Value: token.Token, Expires: expiration, Path: "/"}
		http.SetCookie(w, &cookie)

		http.Redirect(w, r, "/profile", http.StatusFound)

		return
	}

	// If a user without a google id exists we can create a new 
	// acount
	exists, err = users.GoogleIDExists(s.db, googleUser.ID)

	if err != nil {
		res.Error = "Oops something terribly went wrong."
		rnd.HTML(w, http.StatusOK, "home", res)
		return
	}

	if !exists {
		var u users.User

		u.Email = googleUser.Email
		u.FirstName = googleUser.GivenName
		u.LastName = googleUser.FamilyName
		u.GoogleID = googleUser.ID

		err := users.Create(s.db, &u)

		if err != nil {
			res.Error = "Error creating user"
			rnd.HTML(w, http.StatusOK, "home", res)
			return
		}

	}

	u, err := users.GetByGoogleID(s.db, googleUser.ID)

	if err != nil {
		res.Error = "internal error"
		rnd.HTML(w, http.StatusOK, "home", res)
		return
	}

	token := auth.Token{UserID: u.ID}

	if err := auth.CreateAccessToken(s.db, &token); err != nil {
		res.Error = "internal error"
		rnd.HTML(w, http.StatusOK, "home", res)
		return
	}

	expiration := time.Now().Add(365 * 24 * time.Hour) // Set to expire in 1 year
	cookie := http.Cookie{Name: "access_token", Value: token.Token, Expires: expiration, Path: "/"}
	http.SetCookie(w, &cookie)

	//If a does not exist, they are sent to onboarding.
	if !exists {
		http.Redirect(w, r, "/onboarding/", http.StatusFound)
		return
	}

	http.Redirect(w, r, "/profile", http.StatusFound)

}
