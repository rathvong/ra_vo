package server

import (
	"log"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/rathvong/ra_vo/service/email"
	"github.com/rathvong/ra_vo/service/users"
)

func (s *Server) GetChangePasswordPage(w http.ResponseWriter, r *http.Request) {
	log.Println("password recover")
	res := View{}

	rnd.HTML(w, http.StatusOK, "recovery", res)

}

func (s *Server) PostForgotLogin(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodPost {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	params := LoginRequest{}

	res := View{}

	r.ParseForm()

	params.Email = r.FormValue("email")

	if params.Email == "" {
		res.Error = "missing password"
		rnd.HTML(w, http.StatusOK, "recovery", res)
		return
	}

	u, err := users.GetByEmail(s.db, strings.ToLower(params.Email))

	if err != nil {
		res.Error = "account is not found."
		rnd.HTML(w, http.StatusOK, "recovery", res)
		return
	}

	ipString, _, _ := net.SplitHostPort(r.RemoteAddr)
	ip := net.ParseIP(ipString)

	rt := users.ResetToken{UserID: u.ID, UserAgent: r.UserAgent(), IPAddress: ip.String(), Email: u.Email}

	err = users.CreateResetToken(s.db, &rt)

	if err != nil {
		res.Error = "error performing that request, please try again"
		rnd.HTML(w, http.StatusOK, "recovery", res)
		return
	}

	emailer := email.Email{}

	e := emailer.BuildForgotPassword(u, &rt)

	if err != nil {
		res.Error = "error performing that request, please try again"
		rnd.HTML(w, http.StatusOK, "recovery", res)
		return
	}

	if err := e.Send(); err != nil {
		res.Error = "error sending email."
		rnd.HTML(w, http.StatusOK, "recovery", res)
		return
	}

	res.Error = "An email has been sent!"
	rnd.HTML(w, http.StatusOK, "recovery", res)

}

func (s *Server) GetPasswordRecoveryPage(w http.ResponseWriter, r *http.Request) {

	token := r.URL.Query().Get("token")

	if token == "" {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	c := struct {
		Token string
		Error string
	}{
		Token: token,
	}

	rnd.HTML(w, http.StatusOK, "changepassword", c)

}

func (s *Server) PostPasswordReset(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()

	token := r.FormValue("token")
	pw1 := r.FormValue("password")
	pw2 := r.FormValue("password2")

	c := struct {
		Token string
		Error string
	}{
		Token: token,
		Error: "",
	}

	if token == "" {
		c.Error = "token is not valid"
		rnd.HTML(w, http.StatusOK, "changepassword", c)
		return
	}

	if pw1 != pw2 {
		c.Error = "password does not match"
		rnd.HTML(w, http.StatusOK, "changepassword", c)
		return
	}

	if len(pw1) < 6 {
		c.Error = "password is less than 6 characters"
		rnd.HTML(w, http.StatusOK, "changepassword", c)
		return
	}

	t, err := users.GetResetToken(s.db, token)

	if err != nil {
		c.Error = "internal error"
		rnd.HTML(w, http.StatusOK, "changepassword", c)
		return
	}

	if t.IsExpired || t.IsConfirmed {
		c.Error = "token is no longer valid"
		rnd.HTML(w, http.StatusOK, "changepassword", c)
		return
	}

	u, err := users.GetByID(s.db, t.UserID)

	if err != nil {
		c.Error = "internal error"
		rnd.HTML(w, http.StatusOK, "changepassword", c)
		return
	}

	u.Password = pw1

	err = users.Update(s.db, u)

	if err != nil {
		c.Error = "internal error"
		rnd.HTML(w, http.StatusOK, "changepassword", c)
		return
	}

	t.IsConfirmed = true
	t.ConfirmedAt = time.Now()

	err = users.UpdateResetToken(s.db, t)
	if err != nil {
		c.Error = "internal error"
		rnd.HTML(w, http.StatusOK, "changepassword", c)
		return
	}

	c.Token = ""
	c.Error = "Password successfully change!"
	rnd.HTML(w, http.StatusOK, "changepassword", c)

}
