package auth

import (
	"errors"
	"log"
	"time"

	"github.com/google/uuid"
	"github.com/rathvong/ra_vo/service/database"
	"github.com/rathvong/ra_vo/service/users"
)
// Tokens are kept for session data retrieval. 
// They will provide access to the site if
// a user is attached.
type Token struct {
	ID        uint64    `json:"id"`
	UserID    uint64    `json:"user_id"`
	Token     string    `json:"token"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func CreateAccessToken(db *database.DB, t *Token) error {
	t.NewUUID()

	if t.UserID == 0 {
		return errors.New("missing user id")
	}

	tx, err := db.Begin()

	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}

		if err := tx.Commit(); err != nil {
			tx.Rollback()
		}
	}()

	if err != nil {
		return err
	}

	qry := `INSERT INTO tokens (
		user_id, token, created_at, updated_at
		) VALUES (
		?, ?, ?, ?
		)`

	t.CreatedAt = time.Now()
	t.UpdatedAt = time.Now()

	res, err := tx.Exec(qry, t.UserID, t.Token, t.CreatedAt, t.UpdatedAt)

	if err != nil {
		log.Printf("auth.CreateAccessToken() SQL: %s Error: %v", qry, err)
		return err
	}

	id, err := res.LastInsertId()

	if err != nil {
		return err
	}

	t.ID = uint64(id)

	return nil
}

func GetAccessToken(db *database.DB, t string) (*Token, error) {

	if t == "" {
		return nil, errors.New("missing access token")
	}

	qry := `SELECT  id,
					user_id,
					token,
					created_at,
					updated_at
			FROM access_tokens
			WHERE token = ?	`

	token := Token{}

	err := db.QueryRow(qry, t).Scan(
		&token.ID,
		&token.UserID,
		&token.Token,
		&token.CreatedAt,
		&token.UpdatedAt,
	)

	if err != nil {
		log.Printf("auth.GetAccessToken() SQL: %s Error:%v", qry, err)
		return nil, err
	}

	return &token, nil
}

func GetUserByAccessToken(db *database.DB, t string) (*users.User, error) {

	if t == "" {
		return nil, errors.New("missing access token")
	}

	u := users.User{}
	qry := `SELECT 
				users.id,
				users.first_name,
				users.last_name,
				users.encrypted_password,
				users.google_id,
				users.email,
				users.telephone,
				users.address,
				users.password_changes,
				users.created_at,
				users.updated_at
			FROM users
			INNER JOIN tokens
			ON tokens.user_id = users.id
			AND tokens.token = ?`

	err := db.QueryRow(qry, t).Scan(
		&u.ID,
		&u.FirstName,
		&u.LastName,
		&u.EncryptedPassword,
		&u.GoogleID,
		&u.Email,
		&u.Telephone,
		&u.Address,
		&u.PasswordChanges,
		&u.CreatedAt,
		&u.UpdatedAt,
	)

	if err != nil {
		log.Printf("token.GetUserByAccessToken SQL: %s Error: %v", qry, err)
	}

	return &u, err
}

// Create an access token for a user
// using a uuid
func (t *Token) NewUUID() {
	token := uuid.New()
	t.Token = token.String()
}
