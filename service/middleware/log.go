package middleware

import (
	"log"
	"net/http"
	"time"
)
// Log all incoming requests
func Log(next http.Handler) http.Handler {
	middleFunc := func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		next.ServeHTTP(w, r)
		end := time.Now()
		log.Printf("[%s] %q %s %v\n", r.Method, r.RequestURI, r.UserAgent(), end.Sub(start))
	}
	return http.HandlerFunc(middleFunc)
}
