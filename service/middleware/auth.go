package middleware

import (
	"log"
	"net/http"

	"github.com/rathvong/ra_vo/service/database"
)
// Authenticate will check if there is an access token present
func Authenticate(db *database.DB, next http.Handler) http.HandlerFunc {
	middleFunc := func(w http.ResponseWriter, r *http.Request) {

		t, err := r.Cookie("access_token")
		if err != nil || t == nil {
			log.Printf("%v token: %v", err, t)
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}

		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(middleFunc)
}
