package users

import (
	"errors"
	"log"
	"strings"
	"time"

	"github.com/rathvong/ra_vo/service/database"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID                uint64    `json:"id"`
	FirstName         string    `json:"first_name"`
	LastName          string    `json:"last_name"`
	Password          string    `json:"-"`
	EncryptedPassword string    `json:"-"`
	GoogleID          string    `json:"google_id"` // tracks gmail associated accounts
	Email             string    `json:"email"`
	Telephone         string    `json:"telephone"`
	Address           string    `json:"address"`
	PasswordChanges   uint      `json:"password_changes"`
	CreatedAt         time.Time `json:"created_at"`
	UpdatedAt         time.Time `json:"updated_at"`
}

func Create(db *database.DB, u *User) error {

	if err := u.validateCreate(); err != nil {
		return err
	}

	tx, err := db.Begin()

	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}

		if err := tx.Commit(); err != nil {
			tx.Rollback()
			return
		}
	}()

	if err != nil {
		return err
	}

	qry := `INSERT INTO users (
		first_name, last_name, encrypted_password, google_id, email, telephone, address, created_at, updated_at
		) VALUES (
			?, ?, ?, ?, ?, ?, ?, ?, ?
		) `

	u.CreatedAt = time.Now()
	u.UpdatedAt = time.Now()
	u.EncryptedPassword, err = HashPassword(u.Password)

	if err != nil {
		return err
	}

	res, err := tx.Exec(qry,
		u.FirstName,
		u.LastName,
		u.EncryptedPassword,
		u.GoogleID,
		u.Email,
		u.Telephone,
		u.Address,
		u.CreatedAt,
		u.UpdatedAt,
	)

	if err != nil {
		log.Printf("Users.Create() SQL: %s Error: %v", qry, err)
	}

	id, err := res.LastInsertId()

	if err != nil {
		return err
	}

	u.ID = uint64(id)

	return nil
}

func (u *User) validateCreate() error {

	if u.Email == "" {
		return errors.New("missing email")
	}

	u.Email = strings.ToLower(u.Email)

	if u.GoogleID != "" {
		if u.Password == "" && u.EncryptedPassword == "" {
			u.Password = u.Email
		}
	} else {
		if u.Password == "" && u.EncryptedPassword == "" {
			return errors.New("missing password")
		}
	}

	return nil
}

func (u *User) validateUpdate() error {

	if u.ID == 0 {
		return errors.New("missing id")
	}

	return u.validateCreate()
}

func Update(db *database.DB, u *User) error {

	if err := u.validateUpdate(); err != nil {
		return err
	}

	tx, err := db.Begin()

	if err != nil {
		return err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}

		if err := tx.Commit(); err != nil {
			tx.Rollback()
			return
		}
	}()

	qry := `UPDATE users SET
		first_name = ?,
		last_name = ?,
		encrypted_password = ?,
		google_id = ?,
		email = ?,
		telephone = ?,
		address = ?,
		created_at = ?,
		updated_at = ?,
		password_changes = ?
		WHERE id = ?
	`

	if u.Password != "" {
		u.EncryptedPassword, err = HashPassword(u.Password)

		if err != nil {
			return err
		}
	}

	// Update the updated time of this row
	u.UpdatedAt = time.Now()

	_, err = tx.Exec(qry,
		u.FirstName,
		u.LastName,
		u.EncryptedPassword,
		u.GoogleID,
		u.Email,
		u.Telephone,
		u.Address,
		u.CreatedAt,
		u.UpdatedAt,
		u.PasswordChanges,
		u.ID,
	)

	if err != nil {
		log.Printf("Users.Update() SQL: %s Error: %v", qry, err)
	}

	return err
}

func GetByID(db *database.DB, id uint64) (*User, error) {

	if id == 0 {
		return nil, errors.New("missing id")
	}

	var u User

	qry := `SELECT 
				id,
				first_name,
				last_name,
				encrypted_password,
				google_id,
				email,
				telephone,
				address,
				password_changes,
				created_at,
				updated_at
			FROM users
			WHERE id = ?`

	err := db.QueryRow(qry, id).Scan(
		&u.ID,
		&u.FirstName,
		&u.LastName,
		&u.EncryptedPassword,
		&u.GoogleID,
		&u.Email,
		&u.Telephone,
		&u.Address,
		&u.PasswordChanges,
		&u.CreatedAt,
		&u.UpdatedAt,
	)

	if err != nil {
		log.Printf("Users.GetByID() SQL: %s Error: %v", qry, err)
	}

	return &u, err
}

func GetByGoogleID(db *database.DB, id string) (*User, error) {

	if id == "" {
		return nil, errors.New("missing email")
	}

	var u User

	qry := `SELECT 
				id,
				first_name,
				last_name,
				encrypted_password,
				google_id,
				email,
				telephone,
				address,
				password_changes,
				created_at,
				updated_at
			FROM users
			WHERE google_id = ?`

	err := db.QueryRow(qry, id).Scan(
		&u.ID,
		&u.FirstName,
		&u.LastName,
		&u.EncryptedPassword,
		&u.GoogleID,
		&u.Email,
		&u.Telephone,
		&u.Address,
		&u.PasswordChanges,
		&u.CreatedAt,
		&u.UpdatedAt,
	)

	if err != nil {
		log.Printf("Users.GetByID() SQL: %s Error: %v", qry, err)
	}

	return &u, err
}

func GetByEmail(db *database.DB, e string) (*User, error) {

	if e == "" {
		return nil, errors.New("missing email")
	}

	var u User

	qry := `SELECT 
				id,
				first_name,
				last_name,
				encrypted_password,
				google_id,
				email,
				telephone,
				address,
				password_changes,
				created_at,
				updated_at
			FROM users
			WHERE email = ?`

	err := db.QueryRow(qry, e).Scan(
		&u.ID,
		&u.FirstName,
		&u.LastName,
		&u.EncryptedPassword,
		&u.GoogleID,
		&u.Email,
		&u.Telephone,
		&u.Address,
		&u.PasswordChanges,
		&u.CreatedAt,
		&u.UpdatedAt,
	)

	if err != nil {
		log.Printf("Users.GetByEmail() SQL: %s Error: %v", qry, err)
	}

	return &u, err
}

func EmailExists(db *database.DB, e string) (bool, error) {
	var exists bool

	e = strings.ToLower(e)

	qry := `SELECT EXISTS(SELECT 1 FROM users WHERE email = ?)`

	err := db.QueryRow(qry, e).Scan(&exists)

	if err != nil {
		return false, err
	}

	return exists, nil
}

func GoogleIDExists(db *database.DB, id string) (bool, error) {
	var exists bool

	qry := `SELECT EXISTS(SELECT 1 FROM users WHERE google_id = ?)`

	err := db.QueryRow(qry, id).Scan(&exists)

	if err != nil {
		return false, err
	}

	return exists, nil
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(hashedPassword, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
	return err == nil
}
