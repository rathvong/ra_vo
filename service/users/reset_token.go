package users

import (
	"errors"
	"log"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/rathvong/ra_vo/service/database"
)

// ResetToken grants a user temporary access to update their
// passwords. The token is no longer valid once it is confirmed or
// has expired. A token has a lifespan of 1 hour.
type ResetToken struct {
	ID          uint64
	UserID      uint64    `json:"user_id"`
	Email       string    `json:"email"`
	IsExpired   bool      `json:"is_expired"`
	IsConfirmed bool      `json:"is_confirmed"`
	ConfirmedAt time.Time `json:"confirmed_at"`
	Token       string    `json:"token"`
	IPAddress   string    `json:"ip_address"`
	UserAgent   string    `json:"user_agent"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

func CreateResetToken(db *database.DB, r *ResetToken) error {

	if r.UserID == 0 {
		return errors.New("Missing user_id")
	}

	if r.Email == "" {
		return errors.New("Missing email")
	}

	tx, err := db.Begin()

	defer func() {

		if err != nil {
			return

		}

		if err := tx.Commit(); err != nil {
			tx.Rollback()
			return
		}
	}()

	if err != nil {
		return err
	}

	r.Email = strings.ToLower(r.Email)
	token := uuid.New()
	r.Token = token.String()
	r.CreatedAt = time.Now()
	r.UpdatedAt = time.Now()

	qry := `INSERT INTO reset_tokens (
		user_id, email, is_confirmed, token, ip_address, user_agent, created_at, updated_at
		) VALUES (
			?, ?, ?, ?, ?, ?, ?, ?
		)`

	res, err := tx.Exec(qry,
		r.UserID,
		r.Email,
		r.IsConfirmed,
		r.Token,
		r.IPAddress,
		r.UserAgent,
		r.CreatedAt,
		r.UpdatedAt,
	)

	if err != nil {
		log.Printf("users.CreatePasswordReset() SQL: %s Error: %v", qry, err)
		return err
	}

	id, err := res.LastInsertId()

	if err != nil {
		return err
	}

	r.ID = uint64(id)

	return nil
}

func UpdateResetToken(db *database.DB, r *ResetToken) error {

	if r.ID == 0 {
		return errors.New("Missing id")
	}

	tx, err := db.Begin()

	defer func() {

		if err != nil {
			tx.Rollback()
			return
		}

		if err := tx.Commit(); err != nil {
			tx.Rollback()
			return
		}

	}()

	if err != nil {
		return err
	}

	qry := `UPDATE reset_tokens SET
		is_confirmed = ?,
		confirmed_at = ?
		WHERE id = ?
	`

	_, err = tx.Exec(qry, r.IsConfirmed, r.ConfirmedAt, r.ID)

	if err != nil {
		log.Printf("users.UpdatePasswordReset() SQL: %s Error: %v", qry, err)
	}

	return err
}

func GetResetToken(db *database.DB, t string) (*ResetToken, error) {

	if t == "" {
		return nil, errors.New("missing token")
	}

	qry := `SELECT  id,
					user_id, 
					email, 
					is_confirmed, 
					confirmed_at,
					token, 
					ip_address, 
					user_agent, 
					created_at, 
					updated_at
			FROM reset_tokens
			WHERE token = ?		
					`

	r := ResetToken{}

	err := db.QueryRow(qry, t).Scan(
		&r.ID,
		&r.UserID,
		&r.Email,
		&r.IsConfirmed,
		&r.ConfirmedAt,
		&r.Token,
		&r.IPAddress,
		&r.UserAgent,
		&r.CreatedAt,
		&r.UpdatedAt,
	)

	if err != nil {
		log.Printf("users.GetResetToken() SQL: %s Error: %v", qry, err)
		return nil, err
	}

	if r.CreatedAt.UTC().Unix() > time.Now().UTC().Add(time.Hour*time.Duration(1)).Unix() {
		r.IsExpired = true
	}

	return &r, err
}
