package database

import (
	"database/sql"
	"log"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

// We need a global database struct to handle queries
type DB struct {
	*sql.DB
}

var once sync.Once

// Connect to a MySQL database location
// url string - location of the database
func Connect() *DB {

	var c MySQL

	c = NewSQL()
	var db *DB

	// We sync the setup once
	once.Do(func() {
		r, _ := sql.Open("mysql", c.String())

		db = &DB{r}

		db.SetMaxOpenConns(87)                 // Sane default
		db.SetMaxIdleConns(0)                  // clear out any idle connections
		db.SetConnMaxLifetime(time.Nanosecond) // prefer to limit and cut the connection as quickly as possible

		// Ping connectiong to database, this will panic if the connection was not a success
		db.PingConnectionToDatabase()

		db.configureUsers(&c)
		db.configureTokens(&c)
		db.configurePasswordReset(&c)

	})

	return db
}

// Ping database for connection
func (db *DB) PingConnectionToDatabase() {
	log.Println("Pinging connection to database...")
	err := db.Ping()

	if err != nil {
		panic(err)
	}
}

// Deletes all tables from the database
func (db *DB) ClearTables() error {

	_, err := db.Exec("DROP TABLE IF EXISTS tokens")

	_, err = db.Exec("DROP TABLE IF EXISTS reset_tokens")

	_, err = db.Exec(" DROP TABLE IF EXISTS  users")

	return err
}

// Creates a users table if it doesn't exist
func (db *DB) configureUsers(c *MySQL) {

	qry := `CREATE TABLE IF NOT EXISTS users (
			id INT AUTO_INCREMENT,
			first_name VARCHAR(40),
			last_name VARCHAR(40),
			encrypted_password VARCHAR(255),
			email VARCHAR(100) NOT NULL,
			token VARCHAR(100) NOT NULL,
			google_id VARCHAR(100),
			telephone VARCHAR(12),
			address TEXT,
			password_changes INT DEFAULT 0,
			created_at TIMESTAMP,
			updated_at TIMESTAMP,
			PRIMARY KEY (id)
		)`

	_, err := db.Exec(qry)

	if err != nil {
		panic(err)
	}

}

// Creates a tokens table if it doesn't exist
func (db *DB) configureTokens(c *MySQL) {

	qry := `CREATE TABLE IF NOT EXISTS tokens (
			id INT AUTO_INCREMENT,
			token VARCHAR(100),
			user_id INT NOT NULL,
			created_at TIMESTAMP,
			updated_at TIMESTAMP,
			PRIMARY KEY (id, token),
			FOREIGN KEY (user_id) REFERENCES users(id)	
		)`

	_, err := db.Exec(qry)

	if err != nil {
		panic(err)
	}

}

// Creates a reset_tokens table if it doesn't exist
func (db *DB) configurePasswordReset(c *MySQL) {
	qry := `CREATE TABLE IF NOT EXISTS reset_tokens (
		id INT AUTO_INCREMENT,
		token VARCHAR(100),
		user_id INT NOT NULL,
		email VARCHAR(100) NOT NULL,
		user_agent VARCHAR(1000),
		is_confirmed BOOLEAN,
		ip_address VARCHAR(100),
		confirmed_at TIMESTAMP,
		created_at TIMESTAMP,
		updated_at TIMESTAMP,
		PRIMARY KEY (id, token),
		FOREIGN KEY (user_id) REFERENCES users(id)	
	)`

	_, err := db.Exec(qry)

	if err != nil {
		panic(err)
	}
}
