package database

import (
	"fmt"

	"github.com/caarlos0/env"
)

type MySQL struct {
	Host     string `env:"SQL_HOST"`
	User     string `env:"SQL_USER"`
	Password string `env:"SQL_PASSWORD"`
	Database string `env:"SQL_DATABASE"`
}

//We need to format the url string to correctly connect to a Heroku MySQL database using TCP
func (cfg MySQL) String() string {
	return fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?parseTime=true", cfg.User, cfg.Password, cfg.Host, cfg.Database)
}

// Parse environmental variables for a new SQL configuration
func NewSQL() MySQL {
	cfg := MySQL{}
	env.Parse(&cfg)
	return cfg
}
