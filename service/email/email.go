package email

import (
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ses"
	"github.com/matcornic/hermes"

	"github.com/rathvong/ra_vo/service/users"
)

const (
	CharSet = "UTF-8"
)

type Email struct {
	From, To, Subject, BodyHTML, BodyText string
	User                                  *users.User
	hermesEmail                           hermes.Email
	Hermes                                *hermes.Hermes
}

func NewHermes() *hermes.Hermes {
	h := hermes.Hermes{
		Product: hermes.Product{
			Name:      "Rath Vong",
			Link:      "https://rathvong.herokuapp.com/",
			Logo:      "http://www.mekongmessenger.com/img/logo.png",
			Copyright: fmt.Sprintf("Copyright © %04d Rath Vong. All rights reserved.", time.Now().Year()),
		},
	}
	return &h
}

func (e *Email) getBodyText() error {
	txt, err := e.Hermes.GeneratePlainText(e.hermesEmail)
	if err != nil {
		return err
	}
	e.BodyText = txt
	return nil
}

func (e *Email) getBodyHTML() error {
	html, err := e.Hermes.GenerateHTML(e.hermesEmail)
	if err != nil {
		return err
	}
	e.BodyHTML = html
	return nil
}

func (e *Email) verify() error {
	if e.From == "" || e.To == "" || e.Subject == "" || e.BodyText == "" || e.BodyHTML == "" {
		return errors.New("struct email does not contain all required properties")
	}
	return nil
}

// An email is sent using AWS SES. A valid
// AWS key id and secret must be present in environment.
// This is a sandboxed account. Some emails accounts that are not validated
// will not receive an email.
func (e Email) Send() error {

	if err := e.getBodyHTML(); err != nil {
		return err
	}

	if err := e.getBodyText(); err != nil {
		return err
	}

	if err := e.verify(); err != nil {
		return err
	}

	awsSession := session.New(&aws.Config{
		Region:      aws.String("us-east-1"),
		Credentials: credentials.NewStaticCredentials(os.Getenv("AWS_ACCESS_KEY_ID"), os.Getenv("AWS_SECRET_KEY_ID"), ""),
	})

	sesSession := ses.New(awsSession)

	sesEmailInput := &ses.SendEmailInput{
		Destination: &ses.Destination{
			ToAddresses: []*string{aws.String(e.User.Email)},
		},
		Message: &ses.Message{
			Body: &ses.Body{
				Html: &ses.Content{
					Data: aws.String(e.BodyHTML)},
				Text: &ses.Content{
					Charset: aws.String(CharSet),
					Data:    aws.String(e.BodyText),
				},
			},
			Subject: &ses.Content{
				Data: aws.String(e.Subject),
			},
		},
		Source: aws.String(e.From),
		ReplyToAddresses: []*string{
			aws.String("donotreply@rathvong.com"),
		},
	}

	_, err := sesSession.SendEmail(sesEmailInput)

	return err
}

// Build body text using hermes because this library has
// a simple but elegant design in their html bodies.
func (e *Email) BuildForgotPassword(u *users.User, rt *users.ResetToken) *Email {

	he := hermes.Email{
		Body: hermes.Body{
			Name: fmt.Sprintf("%s %s", u.FirstName, u.LastName),
			Intros: []string{
				fmt.Sprintf("You have received this email because a password reset request for %s was received.", u.Email),
			},
			Actions: []hermes.Action{
				{
					Instructions: "Click the button below to reset your password:",
					Button: hermes.Button{
						Color: "#DC4D2F",
						Text:  "Reset your password",
						Link:  fmt.Sprintf("https://rathvong.herokuapp.com/u/resetpassword/?token=%s", rt.Token),
					},
				},
			},
			Outros: []string{
				"If you did not request a password reset, no further action is required on your part.",
				fmt.Sprintf("IP Address: %s", rt.IPAddress),
				fmt.Sprintf("User Agent: %s", rt.UserAgent),
			},
			Signature: "Thanks",
		},
	}

	e.From = "rathvong@mekongmessenger.com"
	e.User = u
	e.To = u.Email
	e.Subject = "Password Reset Request"
	e.hermesEmail = he
	e.Hermes = NewHermes()
	return e
}
